from scripts.app_constants.Site_Configurations import read_configurations
from scripts.app_core.Alert_Services import make_connection
from scripts.app_core.Alert_Services import message_body
from scripts.app_core.Alert_Services import email_services
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger

scheduler = BlockingScheduler(daemon=True)


def main():
    site_data, parser = read_configurations()
    folder_data = make_connection(site_data, parser)
    list_of_dict = []
    max_file_count = site_data["max_file_count"]
    for data in folder_data:
        server_name = data[0]
        host = data[1]
        folder_path = data[2]
        file_count = data[3]
        if file_count >= max_file_count:
            each_dict = message_body(server_name, host, folder_path, file_count)
            list_of_dict.append(each_dict)
    if list_of_dict:
        email_services(list_of_dict, site_data)


if __name__ == '__main__':
    scheduler.add_job(main, CronTrigger.from_crontab('0 */6 * * *'), max_instances=10,
                      misfire_grace_time=4)
    while True:
        print("Execution Started")
        scheduler.start()
