import logging.handlers
from logging.handlers import RotatingFileHandler
from scripts.app_constants.Site_Configurations import read_logging_configurations

base_path, log_level = read_logging_configurations()

file_size = 10000000
file_count = 5
service_name = "GLensServer"
LOG_FILE_UPLOADER = base_path + f'{service_name}'
__logger__ = logging.getLogger(service_name + '_Client')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', "%Y-%m-%d %H:%M:%S")
__logger__.setLevel(log_level)
handler = RotatingFileHandler(LOG_FILE_UPLOADER + '.log',
                              maxBytes=int(file_size),
                              backupCount=int(file_count))
handler.setFormatter(formatter)
__logger__.addHandler(handler)
__logger__.debug('Logger Initialized')


def get_logger():
    return __logger__
