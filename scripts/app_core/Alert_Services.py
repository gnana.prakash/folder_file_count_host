"""
AUTHOR: GNANA PRAKASH INT-561
"""
import fabric
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from scripts.app_logging.server_logging import get_logger
from csv import DictWriter
import pandas as pd

logger = get_logger()


def make_connection(site_data, parser):
    number_of_servers = site_data["number_of_servers"]
    counter = 1
    folder_data = []
    while counter <= number_of_servers:
        temp_list = []
        server_id = "server" + str(counter)
        host = parser.get(server_id, "host")
        port = parser.get(server_id, "port")
        user = parser.get(server_id, "user")
        password = parser.get(server_id, "password")
        folder_path = parser.get(server_id, "folder_path")
        server_name = parser.get(server_id, "server_name")
        file_count = 0
        try:
            connection = fabric.Connection(host=str(host), port=port, user=str(user),
                                           connect_kwargs={'password': str(password)})
            logger.info(f"Connection made to the host server Successfully")
            try:
                cmd = str("cd " + str(folder_path) + " && ls |wc -l")
                data = connection.run(cmd, shell=True, hide=True)
                new = str(data).splitlines()[2:-2]
                file_count = int(new[0])
                logger.info(f"Folder_Path: {folder_path} found")
            except Exception as e:
                logger.exception(f"Exception in fetching the data from the folder_path: {str(e)}")
        except Exception as e:
            logger.exception(f"Exception raised in connection: {str(e)}")
        logger.info(f"File Count in the folder_path: {folder_path} is {file_count}")
        temp_list.append(server_name)
        temp_list.append(host)
        temp_list.append(folder_path)
        temp_list.append(file_count)
        folder_data.append(temp_list)
        counter = counter + 1
    return folder_data


def email_services(list_of_dict, site_data):
    try:
        from_address = site_data["email_sent_from"].lstrip('"').rstrip('"')
        password = site_data["email_login_password"].lstrip('"').rstrip('"')
        to_address = site_data["email_sent_to"]
        cc_address = site_data["cc_address"]

        with open('E:\Folder_file_count_host\spreadsheet.csv', 'w') as outfile:
            writer = DictWriter(outfile, ('server_name', 'host', 'folder_path', 'file_count'))
            writer.writeheader()
            writer.writerows(list_of_dict)

        spreadsheet = pd.read_csv('E:\Folder_file_count_host\spreadsheet.csv')

        final_excel = pd.ExcelWriter('E:\Folder_file_count_host\spreadsheet.xlsx')
        spreadsheet.to_excel(final_excel, index=False)
        final_excel.save()

        body = "Hi," + "\n" + " PFA the number of files in the upload folder exceeding the threshold." + "\n" +"\n" +\
               "Please look into the issue and resolve it." + "\n" + "\n" +\
               "Thank You."+"\n"+"GLens Alert Bot"
        msg = MIMEMultipart()

        msg['Subject'] = 'ALERT: File Count Exceeding Threshold'
        msg['From'] = from_address
        msg['To'] = to_address
        msg['CC'] = cc_address
        msg.attach(MIMEText(body, 'plain'))
        with open("E:\Folder_file_count_host\spreadsheet.xlsx", 'rb') as file:
            msg.attach(MIMEApplication(file.read(), Name="File_count_exceed.xlsx"))

        try:
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(from_address, password)
            logger.info(f"Login to the Email account Successfully to send email")
            server.send_message(msg)
            logger.info(f"Alert Email Sent Successfully")
            server.quit()
        except Exception as e:
            logger.exception(f"Exception raised in Authentication: {str(e)}")
    except Exception as e:
        logger.exception(f"Exception raised due to: {str(e)}")

    return


def message_body(server_name, host, folder_path, file_count):
    each_dict = dict()
    each_dict["server_name"] = server_name
    each_dict["host"] = host
    each_dict["folder_path"] = folder_path
    each_dict["file_count"] = file_count

    return each_dict
