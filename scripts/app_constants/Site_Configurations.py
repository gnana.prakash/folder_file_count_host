from configparser import ConfigParser

CONFIGURATION_FILE = r"conf/Configuration.conf"


def read_configurations():
    parser = ConfigParser()
    parser.read(CONFIGURATION_FILE)
    host = parser.get('site_details', 'host')
    port = parser.get('site_details', 'port')
    user = parser.get('site_details', 'user')
    password = parser.get('site_details', 'password')
    folder_path = parser.get('site_details', 'folder_path')
    number_of_servers = int(parser.get('site_details', 'number_of_servers'))
    max_file_count = int(parser.get('site_details', 'max_file_count'))
    email_sent_from = parser.get('site_details', 'email_sent_from')
    email_sent_to = parser.get('site_details', 'email_sent_to')
    email_login_password = parser.get('site_details', 'email_login_password')
    cc_address = parser.get('site_details', 'cc_address')
    site_details = {"host": host, "port": port, "user": user, "password": password, "folder_path": folder_path,
                    "number_of_servers": number_of_servers, "max_file_count": max_file_count,
                    "email_sent_from": email_sent_from ,
                    "email_sent_to": email_sent_to, 'email_login_password': email_login_password,
                    'cc_address': cc_address
                    }
    return site_details, parser


def read_logging_configurations():
    parser = ConfigParser()
    parser.read(CONFIGURATION_FILE)
    log_level = parser.get('site_details', 'log_level')
    basepath = parser.get('site_details', 'basepath')
    return basepath, log_level
